﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Text.RegularExpressions;

namespace SaveWork.Modules
{
    class LoadCertInformation
    {
        public string Organization = "";
        public string Date = "";
        public string Name = "";

        public LoadCertInformation()
        {
            var store = new X509Store(StoreName.My, StoreLocation.CurrentUser);
            store.Open(OpenFlags.ReadOnly);
            string Data = "";
            long Tics = 0;
            foreach (var c in store.Certificates)
            {
                if (Tics < c.NotBefore.Ticks)
                {
                    Tics = c.NotBefore.Ticks;
                    Data = c.Subject;
                    Date = c.NotBefore.Date.ToShortDateString();
                }
            }
            Name = GetField(Data, "CN=[^,]*", 3);
            Organization = DeleteDoubleSleshas(GetField(Data, "O=[^L]*", 2, 2));
        }


        private string GetField(string Data, string Regex, int Offset, int EndOffset = 0)
        {
            Regex regex = new Regex(Regex);
            Match match = regex.Match(Data);
            if (match.Length == 0) return "";
            EndOffset = match.ToString().Length - EndOffset - Offset;
            return match.ToString().Substring(Offset, EndOffset).Trim();
        }

        private string DeleteDoubleSleshas(string Text)
        {
            string res = "";
            bool doubleslesh = false;
            foreach (char item in Text)
            {
                if (item == '"')
                {
                    if (doubleslesh)
                    {
                        doubleslesh = false;
                        continue;
                    }
                    doubleslesh = true;
                }

                res += item;
            }

            return res;
        }

    }
}
